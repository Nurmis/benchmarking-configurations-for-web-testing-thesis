Some tutorials for installing and setting up environments, general information: http://www.seleniumhq.org/docs/03_webdriver.jsp#setting-up-a-selenium-webdriver-project

Java: http://www.softwaretestinghelp.com/webdriver-eclipse-installation-selenium-tutorial-9/

Python: http://selenium-python.readthedocs.org/installation.html
http://seleniumhq.github.io/selenium/docs/api/py/

Ruby: https://github.com/SeleniumHQ/selenium/wiki/Ruby-Bindings

C#: http://learnseleniumtesting.com/basic-webdriver-and-c-sharp-script-for-beginners/


Downloads for Webdriver executables:

Chrome: https://sites.google.com/a/chromium.org/chromedriver/

Opera: https://github.com/operasoftware/operachromiumdriver

Internet Explorer: https://github.com/SeleniumHQ/selenium/wiki/InternetExplorerDriver