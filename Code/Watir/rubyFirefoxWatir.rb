require "rubygems"
require "test/unit"
require "securerandom"
require "watir-webdriver"
 
class GraduTests < Test::Unit::TestCase 
  def setup 
    $b = Watir::Browser.new :firefox
  end
   
  def teardown 
    $b.close
  end
  
  def test_that_featured_themes_exist_on_the_home
    $b.goto "https://addons.allizom.org/en-US/firefox/"
	title = $b.element(:css => "#featured-themes h2").text
	assert_equal 'Featured Themes See all »', title
  end
  
  def test_addons_author_link
	$b.goto "https://addons.allizom.org/en-US/firefox/"
	$b.element(:xpath => "//*[@id='promos']").wait_until_present
	
	addon = $b.element(:css => "#featured-extensions > ul > section:nth-child(1) > li:nth-child(1) > div > div.summary > a > h3")
	author_element = $b.element(:css => "#featured-extensions > ul > section:nth-child(1) > li:nth-child(1) > div > div.more > div.byline > a")
	mainpage_author_name = author_element.attribute_value("title")
	
	addon.hover
	author_element.hover
	author_element.click
	
	userpage_element = $b.element(:css => "#breadcrumbs > ol > li:nth-child(2)")
	userpage_author_name = userpage_element.element(:tag_name => 'span')
	
	assert $b.url.include? 'user'
	assert_equal userpage_author_name.text, mainpage_author_name
  end
  
  def test_that_checks_if_the_extensions_are_sorted_by_most_user
	user_counts = Array.new
	$b.goto "https://addons.allizom.org/en-US/firefox/"
	$b.element(:xpath => "//*[@id='promos']").wait_until_present
	
	$b.element(:css => "#featured-extensions > h2 > a").click
	$b.element(:css => "#sorter > ul > li:nth-child(2) > a").click
	
	Watir::Wait.until { $b.element(:css =>"#page > section.primary > h1").text.include? 'Most Popular Extensions'}
	extensions = $b.elements(:css => "div.adu")
	for element in extensions do
		user_counts.push(element.text.delete('users').gsub(',', '').to_i)
	end
	user_counts_sorted = user_counts.sort.reverse
	
	assert $b.url.include? 'sort=user'
	assert_equal user_counts, user_counts_sorted
  end
  
  def test_that_checks_if_the_subscribe_link_exists
	$b.goto "https://addons.allizom.org/en-US/firefox/"
	$b.element(:xpath => "//*[@id='promos']").wait_until_present
	
	$b.element(:css => "#featured-extensions > h2 > a").click
	subscribe_element = $b.element(:css => "#subscribe")
	
	assert subscribe_element.text.include? 'Subscribe'
  end
  
  def test_featured_tab_is_highlighted_by_default
	$b.goto "https://addons.allizom.org/en-US/firefox/"
	$b.element(:xpath => "//*[@id='promos']").wait_until_present
	
	$b.element(:css => "#featured-collections > h2 > a").click
	default_selected_tab = $b.element(:css => "#sorter li.selected")
	
	assert_equal default_selected_tab.text, 'Featured'
  end
  
  def test_create_and_delete_collection
	$b.goto "https://addons.allizom.org/en-US/firefox/"
	$b.element(:xpath => "//*[@id='promos']").wait_until_present
	
	$b.element(:xpath => "//*[@id='aux-nav']/ul/li[1]/a[2]").click
	$b.element(:id => 'id_username').send_keys('username')
	$b.element(:id => 'id_password').send_keys('password', :enter)
	
	$b.element(:css => "#collections > a").click
	$b.element(:css => "#side-nav > section:nth-child(4) > p:nth-child(3) > a").click
	
	uuid = SecureRandom.uuid
	time = Time.now.to_i
	collection_name = uuid[0, 29 - time.to_s.length] + time.to_s
	
	$b.element(:id => 'id_name').send_keys(collection_name)
	$b.element(:id => 'id_description').send_keys(collection_name)
	$b.element(:css => "#main-wrapper > div.section > div.primary > div > div > form > p:nth-child(6) > input[type='submit']").click
	
	$b.element(:xpath => "//*[@id='main-wrapper']/div[1]/div[3]/h2").wait_until_present
	assert_equal $b.element(:css => ".notification-box.success h2").text, "Collection created!"
	assert_equal $b.element(:css => ".collection > span").text, collection_name
	
	$b.element(:css => ".delete").click
	$b.element(:css => ".section > form > button").click
	collections = $b.elements(:css => ".featured-inner div.item")
	
	if not collections
		pass
	else
		for collection_element in collections
			assert collection_name != collection_element.text
		end
	end
  end
  
  def test_that_clicking_the_amo_logo_loads_home_page
	$b.goto "https://addons.allizom.org/en-US/firefox/"
	$b.element(:xpath => "//*[@id='promos']").wait_until_present
	
	amo_logo_element = $b.element(:css => ".site-title")
	assert amo_logo_element.visible?
	amo_logo_element.click
	$b.element(:xpath => "//*[@id='promos']").wait_until_present
	
	title_element = $b.element(:css => ".site-title")
	assert title_element.visible?
	assert $b.url.include? 'https://addons.allizom.org/en-US/'
  end
  
  def test_that_other_applications_link_has_tooltip
	$b.goto "https://addons.allizom.org/en-US/firefox/"
	$b.element(:xpath => "//*[@id='promos']").wait_until_present
	
	other_applications_element = $b.element(:id => "other-apps")
	tooltip = other_applications_element.attribute_value('title')
	assert_equal tooltip, 'Find add-ons for other applications'
  end
  
  def test_the_search_box_exist
	$b.goto "https://addons.allizom.org/en-US/firefox/"
	$b.element(:xpath => "//*[@id='promos']").wait_until_present
	
	search_textbox = $b.element(:id => "search-q")
	assert search_textbox.visible?
  end
  
  def test_that_new_review_is_saved
	$b.goto "https://addons.allizom.org/en-US/firefox"
	$b.element(:xpath => "//*[@id='promos']").wait_until_present
	
	$b.element(:xpath => "//*[@id='aux-nav']/ul/li[1]/a[2]").click
	$b.element(:id => 'id_username').send_keys('username')
	$b.element(:id => 'id_password').send_keys('password', :enter)
	user_logged_in_element = $b.element(:css => "#aux-nav .account a.user")
	assert $b.url.include? 'https://addons.allizom.org/en-US/firefox'
	assert user_logged_in_element.visible?
	
	$b.goto "https://addons.allizom.org/en-US/firefox/addon/firebug/"
	$b.element(:xpath => "//*[@id='add-review']").wait_until_present
	$b.element(:xpath => "//*[@id='add-review']").click
	
	timeStamp = Time.now.to_s
	body = 'Automatic addon review by Selenium tests ' + timeStamp
	timeStamp2 = Time.now.strftime("%Y-%m-%d")
	
	$b.element(:id => "review-box").wait_until_present
	$b.element(:id => "id_review_body").send_keys(body)
	ratingElement = $b.element(:css => ".ratingwidget.stars.stars-0 > label")
	ratingElement.hover
	ratingElement.click
	
	$b.element(:css => "#review-box input[type=submit]").click
	$b.element(:css => "#aux-nav > ul > li.account > a").click #click myprofile
	
	reviews = $b.elements(:css => '#reviews > div')
	recent_review = reviews[0]
	assert recent_review.element(:css => "span.stars").text.include? '1'
	timeStamp2 = Time.now.strftime("%Y-%m-%d")
	assert recent_review.text.include? timeStamp2
	
	delete_review = $b.element(:css => '.delete-review').click
	Watir::Wait.until(10) { $b.element(:css =>'.item-actions > li:nth-child(2)').text.include? 'Marked for deletion'}
  end
  
  def test_that_searching_for_cool_returns_results_with_cool_in_their_name_description
	$b.goto "https://addons.allizom.org/en-US/firefox"
	$b.element(:xpath => "//*[@id='promos']").wait_until_present
	
	search_term = "cool"
	$b.element(:id => 'search-q').send_keys(search_term)
	$b.element(:css => "#search > button").click
	
	assert $b.elements(:css => "p.no-results").length == 0
	search_results = $b.elements(:css => "div.items div.item.addon")
	
	i = 0    #no each_with_index for ElementCollection
	search_results.each do
		begin
			assert search_results[i].text.downcase.include? search_term
		rescue MiniTest::Assertion
			search_results[i].element(:css => 'div.info > h3 > a').click
			check_comments = $b.elements(:css => "#developer-comments")
			dev_comments = ''
			if check_comments.length != 0
				$b.element(:css => "#developer-comments h2 a").click
				$b.element(:css => "#developer-comments div.content").wait_until_present
				dev_comments = $b.element(:css => "#developer-comments div.content").text
			end
			search_range = $b.element(:css => "div.prose").text + dev_comments
			puts search_range
			assert search_range.downcase.include? search_term
			$b.back
		ensure
			i = i+1
		end
	end
  end
  
  def test_sorting_by_newest
	dates = Array.new
	$b.goto "https://addons.allizom.org/en-US/firefox"
	$b.element(:xpath => "//*[@id='promos']").wait_until_present
	
	search_term = "firebug"
	$b.element(:id => 'search-q').send_keys(search_term)
	$b.element(:css => "#search > button").click
	
	$b.element(:xpath => "//div[@id='sorter']//li/a[normalize-space(text())='Newest']").click
	$b.element(:css => '.updating.tall').wait_while_present
	search_results = $b.elements(:css => "div.items div.item.addon")
	i = 0
	search_results.each do
		date_str = search_results[i].element(:css => 'div.info > div.vitals > div.updated').text.sub('Added ','')
		date = Date.parse date_str
		dates.push(date.to_s.gsub('-', '').to_i)
		i = i+1
	end
	dates_sorted = dates.sort.reverse
	
	assert_equal dates, dates_sorted
	assert $b.url.include? 'sort=created'
  end
  
  def test_that_searching_for_a_tag_returns_results
	$b.goto "https://addons.allizom.org/en-US/firefox"
	$b.element(:xpath => "//*[@id='promos']").wait_until_present
	
	search_term = "development"
	$b.element(:id => 'search-q').send_keys(search_term)
	$b.element(:css => "#search > button").click
	
	search_count_without_tag = $b.elements(:css => "div.items div.item.addon").length
	assert search_count_without_tag > 0
	
	$b.element(:css => 'li#tag-facets h3').click
	$b.element(:css => '#tag-facets > ul > li:nth-child(4) > a').click
	
	assert search_count_without_tag => $b.elements(:css => "div.items div.item.addon").length
  end
  
  def test_that_verifies_the_url_of_the_statistics_page
	$b.goto "https://addons.allizom.org/en-US/firefox/addon/firebug/"
	
	$b.element(:css => '#daily-users > a.stats').click
	Watir::Wait.until { $b.title.include? "Statistics" }
	assert $b.url.include? '/statistics'
  end
  
  def test_the_recently_added_section
	dates = Array.new
	$b.goto "https://addons.allizom.org/en-US/"
	$b.element(:xpath => "//*[@id='promos']").wait_until_present
	
	$b.element(:css => '#themes > a').click
	recently_added = $b.elements(:css => "#personas-created .persona-small")
	
	i = 0
	recently_added.each do
		date_str = recently_added[i].text.gsub('Added ', '')
		date = Date.parse date_str
		dates.push(date.to_s.gsub('-', '').to_i)
		i = i+1
	end
	dates_sorted = dates.sort.reverse
	
	assert $b.url.include? 'themes'
	assert_equal recently_added.length, 6
	assert dates == dates_sorted
  end
  
  def test_that_most_popular_link_is_default
	$b.goto "https://addons.allizom.org/en-US/"
	$b.element(:xpath => "//*[@id='promos']").wait_until_present
	
	themes_menu = $b.element(:css => '#themes')
	complete_themes_menu = $b.element(:css => '#site-nav div > a.complete-themes > b')
	themes_menu.hover
	complete_themes_menu.hover
	complete_themes_menu.click
	
	explore_filter = ''
	i = 0
	links = $b.elements(:css => "#side-explore a")
	links.each do
		link_examined = links[i].style("font-weight")
		if link_examined == 'bold' or link_examined.to_i > 400
			explore_filter = links[i].text
		end
		i = i+1
	end
	
	assert $b.url.end_with? '/complete-themes/'
	assert_equal explore_filter, 'Most Popular'
  end
  
  def test_that_external_link_leads_to_addon_website
	$b.goto "https://addons.allizom.org/en-US/firefox/addon/memchaser/"
	
	website_link = $b.element(:css => ".links a.home").attribute_value("href")
	assert website_link != ''
	results = website_link.split("https")
	result = results[2]
	result.slice!(0..3)
	
	$b.element(:css => ".links a.home").click
	assert $b.url.include? result
  end
  
  def test_user_can_access_the_edit_profile_page
	$b.goto "https://addons.allizom.org/en-US/"
	$b.element(:xpath => "//*[@id='promos']").wait_until_present
	
	$b.element(:xpath => "//*[@id='aux-nav']/ul/li[1]/a[2]").click
	$b.element(:id => 'id_username').send_keys('username')
	$b.element(:id => 'id_password').send_keys('password', :enter)
	user_logged_in_element = $b.element(:css => "#aux-nav .account a.user")
	assert $b.url.include? 'https://addons.allizom.org/en-US/firefox'
	assert user_logged_in_element.visible?
	
	hover_profile = $b.element(:css => "#aux-nav .account a.user")
	click_profile = $b.element(:css => "#aux-nav .account ul").element(:css => " li:nth-child(2) a")
	hover_profile.hover
	click_profile.hover
	click_profile.click
	
	assert $b.url.include? "/users/edit"
	assert_equal "My Account", $b.element(:css => "#acct-account > legend").text
	assert_equal "Profile", $b.element(:css => "#profile-personal > legend").text
	assert_equal "Details", $b.element(:css => "#profile-detail > legend").text
	assert_equal "Notifications", $b.element(:css => "#acct-notify > legend").text
  end
  
  def test_that_make_contribution_button_is_clickable_while_user_is_logged_in
	$b.goto "https://addons.allizom.org/en-US/firefox/addon/firebug/"
	
	$b.element(:xpath => "//*[@id='aux-nav']/ul/li[1]/a[2]").click
	$b.element(:id => 'id_username').send_keys('username')
	$b.element(:id => 'id_password').send_keys('password', :enter)
	user_logged_in_element = $b.element(:css => "#aux-nav .account a.user")
	assert user_logged_in_element.visible?
	
	$b.element(:css => "#contribute-button").click
	assert $b.element(:id =>'contribute-confirm').visible?
	assert_equal "Make Contribution", $b.element(:id =>'contribute-confirm').text
  end
  
  def test_the_logout_link_for_logged_in_users
	$b.goto "https://addons.allizom.org/en-US/"
	$b.element(:xpath => "//*[@id='promos']").wait_until_present
	
	$b.element(:xpath => "//*[@id='aux-nav']/ul/li[1]/a[2]").click
	$b.element(:id => 'id_username').send_keys('username')
	$b.element(:id => 'id_password').send_keys('password', :enter)
	user_logged_in_element = $b.element(:css => "#aux-nav .account a.user")
	assert $b.url.include? 'https://addons.allizom.org/en-US/'
	assert user_logged_in_element.visible?
	
	discovery_pane = $b.element(:css => '#aux-nav > ul > li.account > a')
	logout_button = $b.element(:css => '#aux-nav > ul > li.account > ul > li.nomenu.logout > a')
	discovery_pane.hover
	logout_button.hover
	logout_button.click
	
	assert $b.url.include? "https://addons.allizom.org/en-US/"
	not_logged = $b.elements(:css => '#aux-nav .account a.user')
	assert_equal 0, not_logged.length
  end
  
end