require "rubygems"
require "test/unit"
require "securerandom"
require "selenium-webdriver"

class GraduTests < Test::Unit::TestCase 

	def setup
		$d = Selenium::WebDriver.for :firefox
	end

	def test_that_featured_themes_exist_on_the_home
		$d.get "https://addons.allizom.org/en-US/firefox/"
		title = $d.find_element(:css => "#featured-themes h2").text
		assert_equal 'Featured Themes See all »', title
	end
	
	def test_addons_author_link
		$d.get "https://addons.allizom.org/en-US/firefox/"
		wait = Selenium::WebDriver::Wait.new(:timeout => 10) # seconds
		element = wait.until { $d.find_element(:xpath => "//*[@id='promos']").displayed? }
		
		addon = $d.find_element(:css => "#featured-extensions > ul > section:nth-child(1) > li:nth-child(1) > div > div.summary > a > h3")
		author_element = $d.find_element(:css => "#featured-extensions > ul > section:nth-child(1) > li:nth-child(1) > div > div.more > div.byline > a")
		mainpage_author_name = author_element.attribute("title")
		
		$d.action.move_to(addon).perform
		$d.action.click(author_element).perform
		
		userpage_element = $d.find_element(:css => "#breadcrumbs > ol > li:nth-child(2)")
		userpage_author_name = userpage_element.find_element(:tag_name =>'span')
		
		assert $d.current_url.include? 'user'
		assert_equal userpage_author_name.text, mainpage_author_name
	end
	
	def test_that_checks_if_the_extensions_are_sorted_by_most_user
		user_counts = Array.new
		$d.get "https://addons.allizom.org/en-US/firefox/"
		wait = Selenium::WebDriver::Wait.new(:timeout => 10) # seconds
		wait.until { $d.find_element(:xpath => "//*[@id='promos']").displayed? }
		
		$d.find_element(:css => "#featured-extensions > h2 > a").click
		$d.find_element(:css => "#sorter > ul > li:nth-child(2) > a").click
		wait.until { $d.find_element(:css => "#page > section.primary > h1").text.include? 'Most Popular Extensions' }
		extensions = $d.find_elements(:css => "div.adu")
		for element in extensions do
			user_counts.push(element.text.delete('users').gsub(',', '').to_i)
		end
		user_counts_sorted = user_counts.sort.reverse
		
		assert $d.current_url.include? 'sort=user'
		assert_equal user_counts, user_counts_sorted
	end
	
	def test_that_checks_if_the_subscribe_link_exists
		$d.get "https://addons.allizom.org/en-US/firefox/"
		wait = Selenium::WebDriver::Wait.new(:timeout => 10) # seconds
		wait.until { $d.find_element(:xpath => "//*[@id='promos']").displayed? }
	
		$d.find_element(:css => "#featured-extensions > h2 > a").click
		subscribe_element = $d.find_element(:css => "#subscribe")
	
		assert subscribe_element.text.include? 'Subscribe'
	end
	
	def test_featured_tab_is_highlighted_by_default
		$d.get "https://addons.allizom.org/en-US/firefox/"
		wait = Selenium::WebDriver::Wait.new(:timeout => 10) # seconds
		wait.until { $d.find_element(:xpath => "//*[@id='promos']").displayed? }
	
		$d.find_element(:css => "#featured-collections > h2 > a").click
		default_selected_tab = $d.find_element(:css => "#sorter li.selected")
	
		assert_equal default_selected_tab.text, 'Featured'
	end
	
	def test_create_and_delete_collection
		$d.get "https://addons.allizom.org/en-US/firefox/"
		wait = Selenium::WebDriver::Wait.new(:timeout => 10) # seconds
		wait.until { $d.find_element(:xpath => "//*[@id='promos']").displayed? }
	
		$d.find_element(:xpath => "//*[@id='aux-nav']/ul/li[1]/a[2]").click
		$d.find_element(:id => 'id_username').send_keys('username')
		$d.find_element(:id => 'id_password').send_keys('password', :enter)
	
		$d.find_element(:css => "#collections > a").click
		$d.find_element(:css => "#side-nav > section:nth-child(4) > p:nth-child(3) > a").click
	
		uuid = SecureRandom.uuid
		time = Time.now.to_i
		collection_name = uuid[0, 29 - time.to_s.length] + time.to_s
	
		$d.find_element(:id => 'id_name').send_keys(collection_name)
		$d.find_element(:id => 'id_description').send_keys(collection_name)
		$d.find_element(:css => "#main-wrapper > div.section > div.primary > div > div > form > p:nth-child(6) > input[type='submit']").click
	
		wait.until { $d.find_element(:xpath => "//*[@id='main-wrapper']/div[1]/div[3]/h2").displayed? }
		assert_equal $d.find_element(:css => ".notification-box.success h2").text, "Collection created!"
		assert_equal $d.find_element(:css => ".collection > span").text, collection_name
	
		$d.find_element(:css => ".delete").click
		$d.find_element(:css => ".section > form > button").click
		collections = $d.find_elements(:css => ".featured-inner div.item")
	
		if not collections
			pass
		else
			for collection_element in collections
				assert collection_name != collection_element.text
			end
		end
	end
	
	def test_that_clicking_the_amo_logo_loads_home_page
		$d.get "https://addons.allizom.org/en-US/firefox/"
		wait = Selenium::WebDriver::Wait.new(:timeout => 10) # seconds
		wait.until { $d.find_element(:xpath => "//*[@id='promos']").displayed? }
	
		amo_logo_element = $d.find_element(:css => ".site-title")
		assert amo_logo_element.displayed?
		amo_logo_element.click
		wait.until { $d.find_element(:xpath => "//*[@id='promos']").displayed? }
	
		title_element = $d.find_element(:css => ".site-title")
		assert title_element.displayed?
		assert $d.current_url.include? 'https://addons.allizom.org/en-US/'
	end
	
	def test_that_other_applications_link_has_tooltip
		$d.get "https://addons.allizom.org/en-US/firefox/"
		wait = Selenium::WebDriver::Wait.new(:timeout => 10) # seconds
		wait.until { $d.find_element(:xpath => "//*[@id='promos']").displayed? }
	
		other_applications_element = $d.find_element(:id => "other-apps")
		tooltip = other_applications_element.attribute('title')
		assert_equal tooltip, 'Find add-ons for other applications'
	end
	
	def test_the_search_box_exist
		$d.get "https://addons.allizom.org/en-US/firefox/"
		wait = Selenium::WebDriver::Wait.new(:timeout => 10) # seconds
		wait.until { $d.find_element(:xpath => "//*[@id='promos']").displayed? }
	
		search_textbox = $d.find_element(:id => "search-q")
		assert search_textbox.displayed?
	end
	
	def test_that_new_review_is_saved
		$d.get "https://addons.allizom.org/en-US/firefox/"
		wait = Selenium::WebDriver::Wait.new(:timeout => 10) # seconds
		wait.until { $d.find_element(:xpath => "//*[@id='promos']").displayed? }
	
		$d.find_element(:xpath => "//*[@id='aux-nav']/ul/li[1]/a[2]").click
		$d.find_element(:id => 'id_username').send_keys('username')
		$d.find_element(:id => 'id_password').send_keys('password', :enter)
		user_logged_in_element = $d.find_element(:css => "#aux-nav .account a.user")
		assert $d.current_url.include? 'https://addons.allizom.org/en-US/firefox'
		assert user_logged_in_element.displayed?
	
		$d.get "https://addons.allizom.org/en-US/firefox/addon/firebug/"
		wait.until { $d.find_element(:xpath => "//*[@id='add-review']").displayed? } 
		$d.find_element(:xpath => "//*[@id='add-review']").click
	
		timeStamp = Time.now.to_s
		body = 'Automatic addon review by Selenium tests ' + timeStamp
		timeStamp2 = Time.now.strftime("%Y-%m-%d")
	
		wait.until { $d.find_element(:id => "review-box").displayed? }
		$d.find_element(:id => "id_review_body").send_keys(body)
		ratingElement = $d.find_element(:css => ".ratingwidget.stars.stars-0 > label")		
		$d.action.move_to(ratingElement).perform
		$d.action.click(ratingElement).perform
		
		$d.find_element(:css => "#review-box input[type=submit]").click
		$d.find_element(:css => "#aux-nav > ul > li.account > a").click #click myprofile
	
		reviews = $d.find_elements(:css => '#reviews > div')
		recent_review = reviews[0]
		assert recent_review.find_element(:css => "span.stars").text.include? '1'
		timeStamp2 = Time.now.strftime("%Y-%m-%d")
		assert recent_review.text.include? timeStamp2
	
		delete_review = $d.find_element(:css => '.delete-review').click
		wait.until { $d.find_element(:css =>'.item-actions > li:nth-child(2)').text.include? 'Marked for deletion'}
	end
	
	def test_that_searching_for_cool_returns_results_with_cool_in_their_name_description
		$d.get "https://addons.allizom.org/en-US/firefox/"
		wait = Selenium::WebDriver::Wait.new(:timeout => 10) # seconds
		wait.until { $d.find_element(:xpath => "//*[@id='promos']").displayed? }
	
		search_term = "cool"
		$d.find_element(:id => 'search-q').send_keys(search_term)
		$d.find_element(:css => "#search > button").click
	
		assert $d.find_elements(:css => "p.no-results").length == 0
		search_results = $d.find_elements(:css => "div.items div.item.addon")
	
		i = 0    #no each_with_index for ElementCollection
		search_results.each do
			begin
				assert search_results[i].text.downcase.include? search_term
			rescue MiniTest::Assertion
				search_results[i].find_element(:css => 'div.info > h3 > a').click
				check_comments = $d.find_elements(:css => "#developer-comments")
				dev_comments = ''
				if check_comments.length != 0
					$d.find_element(:css => "#developer-comments h2 a").click
					wait.until { $d.find_element(:css => "#developer-comments div.content").displayed? }
					dev_comments = $d.find_element(:css => "#developer-comments div.content").text
				end
				search_range = $d.find_element(:css => "div.prose").text + dev_comments
				assert search_range.downcase.include? search_term
				$d.back
			ensure
				i = i+1
			end
		end
	end
	
	def test_sorting_by_newest
		dates = Array.new
		wait = Selenium::WebDriver::Wait.new(:timeout => 10) # seconds
		$d.get "https://addons.allizom.org/en-US/firefox"
		wait.until { $d.find_element(:xpath => "//*[@id='promos']").displayed? }
	
		search_term = "firebug"
		$d.find_element(:id => 'search-q').send_keys(search_term)
		$d.find_element(:css => "#search > button").click
	
		$d.find_element(:xpath => "//div[@id='sorter']//li/a[normalize-space(text())='Newest']").click
		wait.until { $d.find_element(:css => '.updating.tall').displayed? }
		sleep 2 #no method for waiting that element is not visible. implicit wait has to be used for ajax to refresh search results
	
		search_results = $d.find_elements(:css => "div.items div.item.addon")
		i = 0
		search_results.each do
			date_str = search_results[i].find_element(:css => 'div.info > div.vitals > div.updated').text.sub('Added ','')
			date = Date.parse date_str
			dates.push(date.to_s.gsub('-', '').to_i)
			i = i+1
		end
		dates_sorted = dates.sort.reverse
	
		assert_equal dates, dates_sorted
		assert $d.current_url.include? 'sort=created'
	end
	
	def test_that_searching_for_a_tag_returns_results
		wait = Selenium::WebDriver::Wait.new(:timeout => 10) # seconds
		$d.get "https://addons.allizom.org/en-US/firefox"
		wait.until { $d.find_element(:xpath => "//*[@id='promos']").displayed? }
	
		search_term = "development"
		$d.find_element(:id => 'search-q').send_keys(search_term)
		$d.find_element(:css => "#search > button").click
	
		search_count_without_tag = $d.find_elements(:css => "div.items div.item.addon").length
		assert search_count_without_tag > 0
	
		$d.find_element(:css => 'li#tag-facets h3').click
		$d.find_element(:css => '#tag-facets > ul > li:nth-child(4) > a').click
	
		assert search_count_without_tag => $d.find_elements(:css => "div.items div.item.addon").length
	end
	
	def test_that_verifies_the_url_of_the_statistics_page
		wait = Selenium::WebDriver::Wait.new(:timeout => 10) # seconds
		$d.get "https://addons.allizom.org/en-US/firefox/addon/firebug/"
	
		$d.find_element(:css => '#daily-users > a.stats').click
		wait.until { $d.title.include? "Statistics" }
	
		assert $d.current_url.include? '/statistics'
	end
	
	def test_the_recently_added_section
		wait = Selenium::WebDriver::Wait.new(:timeout => 10) # seconds
		dates = Array.new
		$d.get "https://addons.allizom.org/en-US/"
		wait.until { $d.find_element(:xpath => "//*[@id='promos']").displayed? }
	
		$d.find_element(:css => '#themes > a').click
		recently_added = $d.find_elements(:css => "#personas-created .persona-small")
	
		i = 0
		recently_added.each do
			date_str = recently_added[i].text.gsub('Added ', '')
			date = Date.parse date_str
			dates.push(date.to_s.gsub('-', '').to_i)
			i = i+1
		end
		dates_sorted = dates.sort.reverse
	
		assert $d.current_url.include? 'themes'
		assert_equal recently_added.length, 6
		assert dates == dates_sorted
	end
	
	def test_that_most_popular_link_is_default
		wait = Selenium::WebDriver::Wait.new(:timeout => 10) # seconds
		$d.get "https://addons.allizom.org/en-US/"
		wait.until { $d.find_element(:xpath => "//*[@id='promos']").displayed? }
	
		themes_menu = $d.find_element(:css => '#themes')
		complete_themes_menu = $d.find_element(:css => '#site-nav div > a.complete-themes > b')
		$d.action.move_to(themes_menu).perform
		$d.action.click(complete_themes_menu).perform
	
		explore_filter = ''
		i = 0
		links = $d.find_elements(:css => "#side-explore a")
		links.each do
			link_examined = links[i].style("font-weight")
			if link_examined == 'bold' or link_examined.to_i > 400
				explore_filter = links[i].text
			end
			i = i+1
		end
	
		assert $d.current_url.end_with? '/complete-themes/'
		assert_equal explore_filter, 'Most Popular'
	end
	
	def test_that_external_link_leads_to_addon_website
		$d.get "https://addons.allizom.org/en-US/firefox/addon/memchaser/"
		
		website_link = $d.find_element(:css => ".links a.home").attribute("href")
		assert website_link != ''
		results = website_link.split("https")
		result = results[2]
		result.slice!(0..3)
		
		$d.find_element(:css => ".links a.home").click
		assert $d.current_url.include? result
	end
	
	def test_user_can_access_the_edit_profile_page
		$d.get "https://addons.allizom.org/en-US/firefox/"
		wait = Selenium::WebDriver::Wait.new(:timeout => 10) # seconds
		wait.until { $d.find_element(:xpath => "//*[@id='promos']").displayed? }
	
		$d.find_element(:xpath => "//*[@id='aux-nav']/ul/li[1]/a[2]").click
		$d.find_element(:id => 'id_username').send_keys('username')
		$d.find_element(:id => 'id_password').send_keys('password', :enter)
		user_logged_in_element = $d.find_element(:css => "#aux-nav .account a.user")
		assert $d.current_url.include? 'https://addons.allizom.org/en-US/firefox'
		assert user_logged_in_element.displayed?
		
		hover_profile = $d.find_element(:css => "#aux-nav .account a.user")
		click_profile = $d.find_element(:css => "#aux-nav .account ul").find_element(:css => " li:nth-child(2) a")
		$d.action.move_to(hover_profile).perform
		$d.action.click(click_profile).perform
		
		assert $d.current_url.include? "/users/edit"
		assert_equal "My Account", $d.find_element(:css => "#acct-account > legend").text
		assert_equal "Profile", $d.find_element(:css => "#profile-personal > legend").text
		assert_equal "Details", $d.find_element(:css => "#profile-detail > legend").text
		assert_equal "Notifications", $d.find_element(:css => "#acct-notify > legend").text
	end
	
	def test_that_make_contribution_button_is_clickable_while_user_is_logged_in
		$d.get "https://addons.allizom.org/en-US/firefox/addon/firebug/"
	
		$d.find_element(:xpath => "//*[@id='aux-nav']/ul/li[1]/a[2]").click
		$d.find_element(:id => 'id_username').send_keys('username')
		$d.find_element(:id => 'id_password').send_keys('password', :enter)
		user_logged_in_element = $d.find_element(:css => "#aux-nav .account a.user")
		assert user_logged_in_element.displayed?
		
		$d.find_element(:css => "#contribute-button").click
		assert $d.find_element(:id =>'contribute-confirm').displayed?
		assert_equal "Make Contribution", $d.find_element(:id =>'contribute-confirm').text
	end
	
	def test_the_logout_link_for_logged_in_users
		$d.get "https://addons.allizom.org/en-US/firefox/"
		wait = Selenium::WebDriver::Wait.new(:timeout => 10) # seconds
		wait.until { $d.find_element(:xpath => "//*[@id='promos']").displayed? }
	
		$d.find_element(:xpath => "//*[@id='aux-nav']/ul/li[1]/a[2]").click
		$d.find_element(:id => 'id_username').send_keys('username')
		$d.find_element(:id => 'id_password').send_keys('password', :enter)
		user_logged_in_element = $d.find_element(:css => "#aux-nav .account a.user")
		assert $d.current_url.include? "https://addons.allizom.org/en-US/"
		assert user_logged_in_element.displayed?
		
		discovery_pane = $d.find_element(:css => '#aux-nav > ul > li.account > a')
		logout_button = $d.find_element(:css => '#aux-nav > ul > li.account > ul > li.nomenu.logout > a')
		$d.action.move_to(discovery_pane).perform
		$d.action.click(logout_button).perform
	
		assert $d.current_url.include? "https://addons.allizom.org/en-US/"
		not_logged = $d.find_elements(:css => '#aux-nav .account a.user')
		assert_equal 0, not_logged.length
	end
	
	def teardown
        $d.quit  
    end
end