﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;

namespace Ie
{
    [TestClass]
    public class Tests
    {
        IWebDriver driver;

        [TestInitialize]
        public void TestSetup()
        {
            driver = new InternetExplorerDriver();
        }
        [TestCleanup]
        public void Cleanup()
        {
            driver.Quit();
        }
        [TestMethod]
        public void TestThatFeaturedThemesExistOnTheHome()
        {
            driver.Navigate().GoToUrl("https://addons.allizom.org/en-US/firefox/");
            IWebElement title = driver.FindElement(By.CssSelector("#featured-themes h2"));

            Assert.AreEqual(title.Text, "Featured Themes See all »");
        }
        [TestMethod]
        public void TestAddonsAuthorLink()
        {
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://addons.allizom.org/en-US/firefox/");
            WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, 10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='promos']"))); //load promomenu

            IWebElement addon = driver.FindElement(By.CssSelector("#featured-extensions > ul > section:nth-child(1) > li:nth-child(1) > div > div.summary > a > h3"));
            IWebElement authorElement = driver.FindElement(By.CssSelector("#featured-extensions > ul > section:nth-child(1) > li:nth-child(1) > div > div.more > div.byline > a"));
            String authorName = authorElement.GetAttribute("title");

            System.Threading.Thread.Sleep(500); // wait half a second for the whole banner animation to show for it not to disturb clicking
            Actions actionChain = new Actions(driver);
            actionChain.MoveToElement(addon).Perform();
            System.Threading.Thread.Sleep(500);
            authorElement.Click();

            wait.Until(ExpectedConditions.TitleContains("User Info"));
            IWebElement userpageElement = driver.FindElement(By.CssSelector("#breadcrumbs > ol > li:nth-child(2)"));
            IWebElement userpageAuthorName = userpageElement.FindElement(By.TagName("span"));

            driver.Manage().Window.Size = new System.Drawing.Size(800, 600);
            Assert.IsTrue(driver.Url.Contains("user"));
            Assert.AreEqual(authorName, userpageAuthorName.Text);
        }
        [TestMethod]
        public void TestThatChecksIfTheExtensionsAreSortedByMostUser()
        {
            driver.Navigate().GoToUrl("https://addons.allizom.org/en-US/firefox/");
            WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, 10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='promos']"))); //load promomenu
            System.Threading.Thread.Sleep(500); // wait half a second for the whole banner animation to show for it not to disturb clicking

            driver.FindElement(By.CssSelector("#featured-extensions > h2 > a")).Click();
            wait.Until(ExpectedConditions.ElementIsVisible((By.CssSelector("#sorter > ul > li:nth-child(2) > a"))));
            driver.FindElement(By.CssSelector("#sorter > ul > li:nth-child(2) > a")).Click();
            ReadOnlyCollection<IWebElement> extensions = driver.FindElements(By.CssSelector("div.adu"));
            var totalCounts = new List<int>();
            for (int i = 0; i < extensions.Count; i++)
            {
                String count = extensions[i].Text.Replace(",", "").Replace(" users", "");
                int c = int.Parse(count);
                totalCounts.Add(c);
            }
            var cmprCounts = totalCounts;
            cmprCounts.Reverse();

            Assert.AreEqual(cmprCounts, totalCounts);
            Assert.IsTrue(driver.Url.Contains("sort=user"));
        }
        [TestMethod]
        public void TestThatChecksIfTheSubscribeLinkExists()
        {
            driver.Navigate().GoToUrl("https://addons.allizom.org/en-US/firefox/");
            WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, 10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='promos']"))); //load promomenu
            System.Threading.Thread.Sleep(500); // wait half a second for the whole banner animation to show for it not to disturb clicking

            driver.FindElement(By.CssSelector("#featured-extensions > h2 > a")).Click();
            IWebElement SubscribeElement = driver.FindElement(By.CssSelector("#subscribe"));
            Assert.IsTrue(SubscribeElement.Text.Contains("Subscribe"));
        }
        [TestMethod]
        public void TestFeaturedTabIsHighlightedByDefault()
        {
            driver.Navigate().GoToUrl("https://addons.allizom.org/en-US/firefox/");
            WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, 10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='promos']"))); //load promomenu

            System.Threading.Thread.Sleep(500); // wait half a second for the whole banner animation to show for it not to disturb clicking
            driver.FindElement(By.CssSelector("#featured-collections > h2 > a")).Click();
            IWebElement DefaultSelectedTab = driver.FindElement(By.CssSelector("#sorter li.selected"));
            Assert.IsTrue(DefaultSelectedTab.Text.Contains("Featured"));
        }
        [TestMethod]
        public void TestCreateAndDeleteCollection()
        {
            driver.Navigate().GoToUrl("https://addons.allizom.org/en-US/firefox/");
            WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, 10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='promos']"))); //load promomenu

            System.Threading.Thread.Sleep(500); // wait half a second for the whole banner animation to show for it not to disturb clicking
            driver.FindElement(By.XPath("//*[@id='aux-nav']/ul/li[1]/a[2]")).Click();
            driver.FindElement(By.Id("id_username")).SendKeys("username");
            driver.FindElement(By.Id("id_password")).SendKeys("password" + Keys.Return);
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.CssSelector("#aux-nav > ul > li.account > a"), "Nurmis"));
            driver.FindElement(By.CssSelector("#collections > a")).Click();
            driver.FindElement(By.CssSelector("#side-nav > section:nth-child(4) > p:nth-child(3) > a")).Click();

            string uuid = System.Guid.NewGuid().ToString();
            string timeStamp = DateTime.Now.TimeOfDay.TotalMilliseconds.ToString().Replace(",", "").Replace("-", "");
            string collectionName = uuid.Substring(0, 28 - timeStamp.Length) + timeStamp;

            driver.FindElement(By.Id("id_description")).SendKeys(collectionName);
            driver.FindElement(By.Id("id_name")).SendKeys(collectionName);
            System.Threading.Thread.Sleep(500); // wait half a second before pressing submit for sendKeys commands to register for ajax
            driver.FindElement(By.CssSelector("#main-wrapper > div.section > div.primary > div > div > form > p:nth-child(6) > input[type='submit']")).Click();

            wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy((By.XPath("//*[@id='main-wrapper']/div[1]/div[3]/h2"))));
            Assert.AreEqual(driver.FindElement(By.CssSelector(".notification-box.success h2")).Text, "Collection created!");
            Assert.AreEqual(driver.FindElement(By.CssSelector(".collection > span")).Text, collectionName);

            driver.FindElement(By.CssSelector(".delete")).Click();
            driver.FindElement(By.CssSelector(".section > form > button")).Click();
            ReadOnlyCollection<IWebElement> collections = driver.FindElements(By.CssSelector(".featured-inner div.item"));

            if (!collections.Any())
            {
                return;
            }
            else
            {
                for (int i = 0; i < collections.Count(); i++)
                {
                    Assert.IsTrue(collectionName != collections[i].Text);
                }
            }
        }
        [TestMethod]
        public void TestThatClickingTheAmoLogoLoadsHomePage()
        {
            driver.Navigate().GoToUrl("https://addons.allizom.org/en-US/firefox/");
            WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, 10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='promos']"))); //load promomenu

            IWebElement amoLogo = driver.FindElement(By.CssSelector(".site-title"));
            Assert.IsTrue(amoLogo.Displayed);
            amoLogo.Click();
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='promos']"))); //load promomenu

            IWebElement titleElement = driver.FindElement(By.CssSelector(".site-title"));
            Assert.IsTrue(titleElement.Displayed);
            Assert.IsTrue(driver.Url.Contains("https://addons.allizom.org/en-US/firefox/"));
        }
        [TestMethod]
        public void TestThatOtherApplicationsLinkHasTooltip()
        {
            driver.Navigate().GoToUrl("https://addons.allizom.org/en-US/firefox/");
            WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, 10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='promos']"))); //load promomenu

            IWebElement otherApplicationsElement = driver.FindElement(By.Id("other-apps"));
            String tooltip = otherApplicationsElement.GetAttribute("title");
            Assert.AreEqual(tooltip, "Find add-ons for other applications");
        }
        [TestMethod]
        public void testTheSearchBoxExist()
        {
            driver.Navigate().GoToUrl("https://addons.allizom.org/en-US/firefox/");
            WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, 10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='promos']"))); //load promomenu

            IWebElement searchTextBox = driver.FindElement(By.Id("search-q"));
            Assert.IsTrue(searchTextBox.Displayed);
        }
        [TestMethod]
        public void TestThatNewReviewIsSaved()
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            driver.Navigate().GoToUrl("https://addons.allizom.org/en-US/firefox/");
            WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, 10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='promos']"))); //load promomenu

            driver.FindElement(By.XPath("//*[@id='aux-nav']/ul/li[1]/a[2]")).Click();
            driver.FindElement(By.Id("id_username")).SendKeys("username");
            driver.FindElement(By.Id("id_password")).SendKeys("password" + Keys.Return);
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.CssSelector("#aux-nav > ul > li.account > a"), "Nurmis"));
            IWebElement userLoggedIn = driver.FindElement(By.CssSelector("#aux-nav .account a.user"));
            Assert.AreEqual(driver.Url.ToString(), "https://addons.allizom.org/en-US/firefox/");
            Assert.IsTrue(userLoggedIn.Displayed);

            driver.Navigate().GoToUrl("https://addons.allizom.org/en-US/firefox/addon/firebug/");
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='add-review']"))); // load review button
            driver.FindElement(By.XPath("//*[@id='add-review']")).Click();

            String timeStamp = DateTime.Now.ToString("MM\\/dd\\/yyyy h\\:mm tt");
            Console.WriteLine(timeStamp);
            String body = "Automatic addon review by Selenium tests " + timeStamp;
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("review-box"))); // load popup box
            driver.FindElement(By.Id("id_review_body")).SendKeys(body);
            js.ExecuteScript("document.querySelector('#review-box > form > p:nth-child(4) > span > label:nth-child(1)').click();");
            js.ExecuteScript("document.querySelector('#review-box input[type=submit]').click();");

            wait.Until(ExpectedConditions.TitleContains("Reviews")); //wait for page to load
            driver.FindElement(By.CssSelector("#aux-nav > ul > li.account > a")).Click(); // click myprofile

            String timeStampToday = DateTime.Now.ToString("MM\\/dd\\/yyyy");
            ReadOnlyCollection<IWebElement> Reviews = driver.FindElements(By.CssSelector("#reviews > div"));
            IWebElement recentReview = Reviews[0];
            Assert.IsTrue(recentReview.FindElement(By.CssSelector("span.stars")).Text.Contains("1"));
            Assert.IsTrue(recentReview.Text.Contains(timeStampToday));

            driver.FindElement(By.CssSelector(".delete-review")).Click(); // click delete review
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.CssSelector(".item-actions > li:nth-child(2)"), "Marked for deletion")); //confirm review is marked for deletion
        }
        [TestMethod]
        public void TestThatSearchingForCoolReturnsResultsWithCoolInTheirNameDescription()
        {
            driver.Navigate().GoToUrl("https://addons.allizom.org/en-US/firefox/");
            WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, 10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='promos']"))); //load promomenu

            String searchTerm = "cool";
            driver.FindElement(By.Id("search-q")).SendKeys(searchTerm);
            driver.FindElement(By.CssSelector("#search > button")).Click();

            ReadOnlyCollection<IWebElement> noResultElement = driver.FindElements(By.CssSelector("p.no-results"));
            Assert.IsTrue(!noResultElement.Any());

            ReadOnlyCollection<IWebElement> searchResults = driver.FindElements(By.CssSelector("div.items div.item.addon"));
            for (int i = 0; i < searchResults.Count; i++)
            {
                try
                {
                    Assert.IsTrue(searchResults[i].Text.ToLower().Contains(searchTerm));
                }
                catch
                { //if you want to run this try football as search term
                    searchResults[i].FindElement(By.CssSelector("div.info > h3 > a")).Click();
                    ReadOnlyCollection<IWebElement> Comments = driver.FindElements(By.CssSelector("#developer-comments"));
                    String devComments = "";
                    if (Comments.Count != 0)
                    {
                        driver.FindElement(By.CssSelector("#developer-comments h2 a")).Click();
                        wait.Until(ExpectedConditions.ElementExists(By.CssSelector("#developer-comments div.content")));
                        devComments = driver.FindElement(By.CssSelector("#developer-comments div.content")).Text;
                    }
                    String searchRange = driver.FindElement(By.CssSelector("div.prose")).Text + devComments;
                    Console.WriteLine(searchRange);
                    Assert.IsTrue(searchRange.ToLower().Contains(searchTerm));
                    driver.Navigate().Back();
                    searchResults = driver.FindElements(By.CssSelector("div.items div.item.addon")); //Because link has been clicked, the elementlist needs to be refreshed
                }
            }
        }
        [TestMethod]
        public void TestSortingByNewest()
        {
            //ArrayList<Long> dates = new ArrayList<Long>();
            //SimpleDateFormat source = new SimpleDateFormat("MMM dd, yyyy", Locale.ENGLISH);

            driver.Navigate().GoToUrl("https://addons.allizom.org/en-US/firefox/");
            WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, 10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='promos']"))); //load promomenu

            String searchTerm = "firebug";
            driver.FindElement(By.Id("search-q")).SendKeys(searchTerm);
            driver.FindElement(By.CssSelector("#search > button")).Click();

            driver.FindElement(By.XPath("//div[@id='sorter']//li/a[normalize-space(text())='Newest']")).Click(); //click sort by newest
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(By.CssSelector(".updating.tall")));
            ReadOnlyCollection<IWebElement> searchResults = driver.FindElements(By.CssSelector("div.items div.item.addon"));
            var dates = new List<DateTime>();
            for (int i = 0; i < searchResults.Count; i++)
            {
                String date = searchResults[i].FindElement(By.CssSelector("div.info > div.vitals > div.updated")).Text.Replace("Added ", "");
                DateTime d = DateTime.Parse(date);
                dates.Add(d);
            }
            var cmprDates = dates;
            cmprDates.Reverse();

            Assert.AreEqual(dates, cmprDates);
            Assert.IsTrue(driver.Url.Contains("sort=created"));
        }
        [TestMethod]
        public void testThatSearchingForATagReturnsResults()
        {
            driver.Navigate().GoToUrl("https://addons.allizom.org/en-US/firefox/");
            WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, 10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='promos']"))); //load promomenu

            String searchTerm = "development";
            driver.FindElement(By.Id("search-q")).SendKeys(searchTerm);
            driver.FindElement(By.CssSelector("#search > button")).Click();

            int countWithoutTag = driver.FindElements(By.CssSelector("div.items div.item.addon")).Count;
            Assert.IsTrue(countWithoutTag > 0);

            driver.FindElement(By.CssSelector("li#tag-facets h3")).Click();
            driver.FindElement(By.CssSelector("#tag-facets > ul > li:nth-child(4) > a")).Click();

            Assert.IsTrue(countWithoutTag >= driver.FindElements(By.CssSelector("div.items div.item.addon")).Count);
        }
        [TestMethod]
        public void TestThatVerifiesTheUrlOfTheStatisticsPage()
        {
            driver.Navigate().GoToUrl("https://addons.allizom.org/en-US/firefox/addon/firebug/");
            WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, 10));

            driver.FindElement(By.CssSelector("#daily-users > a.stats")).Click();
            wait.Until(ExpectedConditions.TitleContains("Statistics"));

            Assert.IsTrue(driver.Url.Contains("/statistics"));
        }
        [TestMethod]
        public void TestTheRecentlyAddedSection()
        {
            driver.Navigate().GoToUrl("https://addons.allizom.org/en-US/firefox/");
            WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, 10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='promos']"))); //load promomenu

            driver.FindElement(By.CssSelector("#themes > a")).Click();
            ReadOnlyCollection<IWebElement> recentlyAdded = driver.FindElements(By.CssSelector("#personas-created .persona-small"));
            var dates = new List<DateTime>();
            for (int i = 0; i < recentlyAdded.Count; i++)
            {
                DateTime d = DateTime.Parse(recentlyAdded[i].Text.Replace("Added ", ""));
                dates.Add(d);
            }

            var cmprDates = dates;
            cmprDates.Reverse();

            Assert.IsTrue(driver.Url.Contains("themes"));
            Assert.AreEqual(6, recentlyAdded.Count);
            Assert.AreEqual(dates, cmprDates);
        }
        [TestMethod]
        public void testThatMostPopularLinkIsDefault()
        {
            driver.Navigate().GoToUrl("https://addons.allizom.org/en-US/firefox/");
            WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, 10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='promos']"))); //load promomenu

            IWebElement themesMenu = driver.FindElement(By.CssSelector("#themes"));
            IWebElement completeThemesMenu = driver.FindElement(By.CssSelector("#site-nav div > a.complete-themes > b"));
            Actions actionChain = new Actions(driver);
            actionChain.MoveToElement(themesMenu).MoveToElement(completeThemesMenu).Click().Perform();
            wait.Until(ExpectedConditions.TitleContains("Most Popular Complete Themes"));
            ReadOnlyCollection<IWebElement> links = driver.FindElements(By.CssSelector("#side-explore a"));
            String exploreFilter = "";
            for (int i = 0; i < links.Count; i++)
            {
                String linkExamined = links[i].GetCssValue("font-weight");
                int link = 0;
                try
                {
                    link = Int32.Parse(linkExamined);
                }
                catch
                {
                    if (linkExamined.Contains("bold"))
                    {
                        exploreFilter = links[i].Text;
                    }
                }
                if (link > 400)
                {
                    exploreFilter = links[i].Text;
                }
            }

            Assert.IsTrue(driver.Url.Contains("themes/"));
            Assert.AreEqual(exploreFilter, "Most Popular");
        }
        [TestMethod]
        public void testThatExternalLinkLeadsToAddonWebsite()
        {
            driver.Navigate().GoToUrl("https://addons.allizom.org/en-US/firefox/addon/memchaser/");

            String WebsiteLink = driver.FindElement(By.CssSelector(".links a.home")).GetAttribute("href");
            Assert.IsTrue(WebsiteLink != "");
            String[] Results = WebsiteLink.Split(new[] { "https" }, StringSplitOptions.None);

            driver.FindElement(By.CssSelector(".links a.home")).Click();
            Assert.IsTrue(driver.Url.Contains(Results[2].Remove(0, 3)));
        }
        [TestMethod]
        public void TestThatUserCamAccessTheEditProfilePage()
        {
            driver.Navigate().GoToUrl("https://addons.allizom.org/en-US/firefox/");
            WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, 10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='promos']"))); //load promomenu

            driver.FindElement(By.XPath("//*[@id='aux-nav']/ul/li[1]/a[2]")).Click();
            driver.FindElement(By.Id("id_username")).SendKeys("username");
            driver.FindElement(By.Id("id_password")).SendKeys("password" + Keys.Return);
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.CssSelector("#aux-nav > ul > li.account > a"), "Nurmis"));
            IWebElement userLoggedInElement = driver.FindElement(By.CssSelector("#aux-nav .account a.user"));
            Assert.IsTrue(driver.Url.Contains("https://addons.allizom.org/en-US/firefox/"));
            Assert.IsTrue(userLoggedInElement.Displayed);

            IWebElement hoverProfile = driver.FindElement(By.CssSelector("#aux-nav .account a.user"));
            IWebElement clickProfile = driver.FindElement(By.CssSelector("#aux-nav .account ul")).FindElement(By.CssSelector(" li:nth-child(2) a"));
            Actions actionChain = new Actions(driver);
            actionChain.MoveToElement(hoverProfile).MoveToElement(clickProfile).Click().Perform();

            wait.Until(ExpectedConditions.TitleContains("Account Settings"));
            Assert.IsTrue(driver.Url.Contains("/users/edit"));
            Assert.AreEqual("My Account", driver.FindElement(By.CssSelector("#acct-account > legend")).Text);
            Assert.AreEqual("Profile", driver.FindElement(By.CssSelector("#profile-personal > legend")).Text);
            Assert.AreEqual("Details", driver.FindElement(By.CssSelector("#profile-detail > legend")).Text);
            Assert.AreEqual("Notifications", driver.FindElement(By.CssSelector("#acct-notify > legend")).Text);
        }
        [TestMethod]
        public void TestThatMakeContributionButtonIsClickableWhileUserIsLoggedIn()
        {
            WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, 10));
            driver.Navigate().GoToUrl("https://addons.allizom.org/en-US/firefox/addon/firebug/");

            driver.FindElement(By.XPath("//*[@id='aux-nav']/ul/li[1]/a[2]")).Click();
            driver.FindElement(By.Id("id_username")).SendKeys("username");
            driver.FindElement(By.Id("id_password")).SendKeys("password" + Keys.Return);
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.CssSelector("#aux-nav > ul > li.account > a"), "Nurmis"));
            IWebElement userLoggedInElement = driver.FindElement(By.CssSelector("#aux-nav .account a.user"));
            Assert.IsTrue(userLoggedInElement.Displayed);

            driver.FindElement(By.CssSelector("#contribute-button")).Click();
            Assert.IsTrue(driver.FindElement(By.Id("contribute-confirm")).Displayed);
            Assert.AreEqual("Make Contribution", driver.FindElement(By.Id("contribute-confirm")).Text);
        }
        [TestMethod]
        public void TestTheLogoutLinkForLoggedInUsers()
        {
            driver.Navigate().GoToUrl("https://addons.allizom.org/en-US/firefox/");
            WebDriverWait wait = new WebDriverWait(driver, new TimeSpan(0, 0, 10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//*[@id='promos']"))); //load promomenu

            driver.FindElement(By.XPath("//*[@id='aux-nav']/ul/li[1]/a[2]")).Click();
            driver.FindElement(By.Id("id_username")).SendKeys("username");
            driver.FindElement(By.Id("id_password")).SendKeys("password" + Keys.Return);
            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.CssSelector("#aux-nav > ul > li.account > a"), "Nurmis"));
            IWebElement userLoggedInElement = driver.FindElement(By.CssSelector("#aux-nav .account a.user"));
            Assert.IsTrue(driver.Url.Contains("https://addons.allizom.org/en-US/firefox/"));
            Assert.IsTrue(userLoggedInElement.Displayed);

            IWebElement discoveryPane = driver.FindElement(By.CssSelector("#aux-nav > ul > li.account > a"));
            IWebElement logoutButton = driver.FindElement(By.CssSelector("#aux-nav > ul > li.account > ul > li.nomenu.logout > a"));
            Actions actionChain = new Actions(driver);
            actionChain.MoveToElement(discoveryPane).MoveToElement(logoutButton).Click().Perform();

            wait.Until(ExpectedConditions.TextToBePresentInElementLocated(By.XPath("//*[@id='aux-nav']/ul/li[1]/a[2]"), "Log in")); //load logout
            Assert.IsTrue(driver.Url.Contains("https://addons.allizom.org/en-US/firefox/"));
            ReadOnlyCollection<IWebElement> userLogged = driver.FindElements(By.CssSelector("#aux-nav .account a.user"));
            Assert.IsTrue(userLogged.Count == 0);
        }
    }
}
