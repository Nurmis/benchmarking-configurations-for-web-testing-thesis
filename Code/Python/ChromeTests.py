from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import unittest
import time
import uuid
import datetime
from time import strptime, mktime

class GraduTests(unittest.TestCase):

	def setUp(self):
		self.driver = webdriver.Chrome()

	def test_that_featured_themes_exist_on_the_home(self):
		driver = self.driver
		driver.get("https://addons.allizom.org/en-US/firefox/")
		
		title = driver.find_element(By.CSS_SELECTOR, "#featured-themes h2")
		self.assertEqual(title.text, u'Featured Themes See all \xbb')

	def test_addons_author_link(self):
		driver = self.driver
		driver.get("https://addons.allizom.org/en-US/firefox/")
		load_promomenu = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='promos']")))
		
		addon = driver.find_element(By.CSS_SELECTOR, "#featured-extensions > ul > section:nth-child(1) > li:nth-child(1) > div > div.summary > a > h3")
		author_element = driver.find_element(By.CSS_SELECTOR, "#featured-extensions > ul > section:nth-child(1) > li:nth-child(1) > div > div.more > div.byline > a")
		mainpage_author_name = author_element.get_attribute("title")
		
		actions = ActionChains(driver)
		actions.move_to_element(addon).perform()
		time.sleep(0.5)
		actions.click(author_element).perform()
		
		userpage_element = driver.find_element(By.CSS_SELECTOR, "#breadcrumbs > ol > li:nth-child(2)")
		userpage_author_name = userpage_element.find_element_by_tag_name('span')
		
		self.assertIn('user', driver.current_url)
		self.assertEqual(userpage_author_name.text, mainpage_author_name)
		
	def test_that_checks_if_the_extensions_are_sorted_by_most_user(self):
		driver = self.driver
		user_counts = []
		descending = False
		driver.get("https://addons.allizom.org/en-US/firefox/")
		load_promomenu = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='promos']")))
		time.sleep(2)
		
		driver.find_element(By.CSS_SELECTOR, "#featured-extensions > h2 > a").click()
		driver.find_element(By.CSS_SELECTOR, "#sorter > ul > li:nth-child(2) > a").click()
				
		load_sort = WebDriverWait(driver, 10).until(EC.text_to_be_present_in_element((By.CSS_SELECTOR, "#page > section.primary > h1"), 'Most Popular Extensions'))
		extensions = driver.find_elements(By.CSS_SELECTOR, 'div.adu')
		for web_element in extensions:
			user_counts.append(int (web_element.text.strip('user').replace(',', '').rstrip()))
		if (user_counts == sorted(user_counts, reverse = True)):
			descending = True
		
		self.assertIn('sort=users', driver.current_url)
		self.assertEqual(True, descending)

	def test_that_checks_if_the_subscribe_link_exists(self):
		driver = self.driver
		driver.get("https://addons.allizom.org/en-US/firefox/")
		load_promomenu = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='promos']")))
		time.sleep(2)
		
		extensions_page = driver.find_element(By.CSS_SELECTOR, "#featured-extensions > h2 > a").click()
		subscribe_element = driver.find_element(By.CSS_SELECTOR, "#subscribe")
		
		self.assertIn('Subscribe', subscribe_element.text)
	
	def test_featured_tab_is_highlighted_by_default(self):
		driver = self.driver
		driver.get("https://addons.allizom.org/en-US/firefox/")
		load_promomenu = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='promos']")))
		time.sleep(2)
		
		featured_collections_page = driver.find_element(By.CSS_SELECTOR, "#featured-collections > h2 > a").click()
		time.sleep(1)
		default_selected_tab = driver.find_element(By.CSS_SELECTOR, "#sorter li.selected")
		
		self.assertEqual(default_selected_tab.text, "Featured")
		
	def test_create_and_delete_collection (self):
		driver = self.driver
		driver.get("https://addons.allizom.org/en-US/firefox/")
		load_promomenu = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='promos']")))
		
		driver.find_element(By.XPATH, "//*[@id='aux-nav']/ul/li[1]/a[2]").click()
		driver.find_element(By.ID, 'id_username').send_keys('username')
		driver.find_element(By.ID, 'id_password').send_keys('password' + Keys.RETURN)
		
		collections_page = driver.find_element(By.CSS_SELECTOR, "#collections > a").click()
		create_collection_page = driver.find_element(By.CSS_SELECTOR, "#side-nav > section:nth-child(4) > p:nth-child(3) > a").click()
		
		collection_uuid = uuid.uuid4().hex
		collection_time = repr(time.time())
		collection_name = collection_uuid[:30 - len(collection_time):] + collection_time
		
		driver.find_element(By.ID, "id_name").send_keys(collection_name)
		driver.find_element(By.ID, "id_description").send_keys(collection_name)
		time.sleep(0.5)
		collection =  driver.find_element(By.CSS_SELECTOR, "#main-wrapper > div.section > div.primary > div > div > form > p:nth-child(6) > input[type='submit']").click()
	
		WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[@id='main-wrapper']/div[1]/div[3]/h2")))
		self.assertEqual(driver.find_element(By.CSS_SELECTOR, ".notification-box.success h2").text, 'Collection created!')
		self.assertEqual(driver.find_element(By.CSS_SELECTOR, ".collection > span").text, collection_name)
		
		driver.find_element(By.CSS_SELECTOR, ".delete").click()
		user_collections = driver.find_element(By.CSS_SELECTOR, ".section > form > button").click()
		collections = driver.find_elements(By.CSS_SELECTOR, ".featured-inner div.item")
		
		if not collections:
			pass
		else:
			for collection_element in range(len(collections)):
				self.assertTrue(collection_name not in collections[collection_element].text)

	def test_that_clicking_the_amo_logo_loads_home_page(self):
		driver = self.driver
		driver.get("https://addons.allizom.org/en-US/firefox/")
		load_promomenu = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='promos']")))
		
		amo_logo_element = driver.find_element(By.CSS_SELECTOR, ".site-title")
		self.assertTrue(amo_logo_element.is_displayed())
		amo_logo_element.click()
		load_promomenu = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='promos']")))
		
		title_element = driver.find_element(By.CSS_SELECTOR, ".site-title")
		self.assertTrue(title_element.is_displayed())
		self.assertIn('https://addons.allizom.org/en-US/', driver.current_url)
	
	def test_that_other_applications_link_has_tooltip(self):
		driver = self.driver
		driver.get("https://addons.allizom.org/en-US/firefox/")
		load_promomenu = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='promos']")))
		
		other_applications_element = driver.find_element(By.ID, "other-apps")
		tooltip = other_applications_element.get_attribute('title')
		self.assertEqual(tooltip, 'Find add-ons for other applications')

	def test_the_search_box_exist(self):
		driver = self.driver
		driver.get("https://addons.allizom.org/en-US/firefox/")
		load_promomenu = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='promos']")))
		
		search_textbox = driver.find_element(By.ID, "search-q")
		self.assertTrue(search_textbox.is_displayed())

	def test_that_new_review_is_saved(self):
		driver = self.driver
		driver.get("https://addons.allizom.org/en-US/firefox/")
		load_promomenu = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='promos']")))
		
		login_page = driver.find_element(By.XPATH, "//*[@id='aux-nav']/ul/li[1]/a[2]").click()
		driver.find_element(By.ID, 'id_username').send_keys('username')
		driver.find_element(By.ID, 'id_password').send_keys('password' + Keys.RETURN)
		user_logged_in_element = driver.find_element(By.CSS_SELECTOR, "#aux-nav .account a.user")
		self.assertTrue('https://addons.allizom.org/en-US/firefox/', driver.current_url)
		self.assertTrue(user_logged_in_element.is_displayed())
		
		driver.get("https://addons.allizom.org/en-US/firefox/addon/firebug/")
		load_review_button = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='add-review']")))
		review_button = driver.find_element(By.XPATH, "//*[@id='add-review']").click()
		
		time = datetime.datetime.now()
		body = 'Automatic addon review by Selenium tests %s' % time
		load_review_popup = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "review-box")))
		driver.find_element(By.ID, "id_review_body").send_keys(body)
		rating_element = driver.find_element((By.CSS_SELECTOR, '.ratingwidget.stars.stars-0 > label')[0],
                                             '%s[data-stars="%s"]' % ((By.CSS_SELECTOR, '.ratingwidget.stars.stars-0 > label')[1], 1))
		actions = ActionChains(driver)
		actions.move_to_element(rating_element).click().perform()
		driver.find_element(By.CSS_SELECTOR, "#review-box input[type=submit]").click()
		
		load_addon_page = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.CSS_SELECTOR, "#aux-nav > ul > li.account > a")))
		my_profile = driver.find_element(By.CSS_SELECTOR, "#aux-nav > ul > li.account > a").click()
		
		reviews = driver.find_elements(By.CSS_SELECTOR, '#reviews > div')
		recent_review = reviews[0]
		self.assertIn('1', recent_review.find_element(By.CSS_SELECTOR, "span.stars").text)
		today = datetime.date.today()
		self.assertIn(unicode(today), recent_review.text)
		
		delete_review = driver.find_element(By.CSS_SELECTOR, '.delete-review').click()
		load_delete_confirmation = WebDriverWait(driver, 10).until(EC.text_to_be_present_in_element((By.CSS_SELECTOR, '.item-actions > li:nth-child(2)'), 'Marked for deletion'))
		
		
	def test_that_searching_for_cool_returns_results_with_cool_in_their_name_description(self):
		driver = self.driver
		driver.get("https://addons.allizom.org/en-US/firefox/")
		load_promomenu = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='promos']")))
		
		search_term = 'cool'
		driver.find_element(By.ID, "search-q").send_keys(search_term)
		driver.find_element(By.CSS_SELECTOR, "#search > button").click()
		
		self.assertTrue(len(driver.find_elements(By.CSS_SELECTOR, "p.no-results")) == 0)
		search_results = driver.find_elements(By.CSS_SELECTOR, "div.items div.item.addon")
		
		for i in range (0, len(search_results)):
			try:
				self.assertIn(search_term, search_results[i].text.lower())
			except:
				dev_comments = '' 				#to get here use football as search term
				details_page = search_results[i].find_element(By.CSS_SELECTOR, 'div.info > h3 > a')
				details_page.click()
				check_comments = driver.find_elements(By.CSS_SELECTOR, "#developer-comments")
				if len(check_comments) != 0:
					driver.find_element(By.CSS_SELECTOR, "#developer-comments h2 a").click()
					load_comments = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.CSS_SELECTOR, "#developer-comments div.content")))
					dev_comments = driver.find_element(By.CSS_SELECTOR, "#developer-comments div.content").text
				search_range = driver.find_element(By.CSS_SELECTOR, "div.prose").text + dev_comments
				self.assertIn(search_term, search_range.lower())
				driver.back()
				search_results = driver.find_elements(By.CSS_SELECTOR, "div.items div.item.addon") #Because a link has been clicked, search result element has changed and needs to be loaded again
	
	def test_sorting_by_newest(self):
		driver = self.driver
		descending = False
		dates = []
		driver.get("https://addons.allizom.org/en-US/firefox/")
		load_promomenu = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='promos']")))
		
		search_term = 'firebug'
		driver.find_element(By.ID, "search-q").send_keys(search_term)
		driver.find_element(By.CSS_SELECTOR, "#search > button").click()
		
		driver.find_element(By.XPATH, "//div[@id='sorter']//li/a[normalize-space(text())='Newest']").click()
		load_sorting = WebDriverWait(driver, 10).until( EC.invisibility_of_element_located((By.CSS_SELECTOR, '.updating.tall')))
		search_results = driver.find_elements(By.CSS_SELECTOR, "div.items div.item.addon")
		for i in range (0, len(search_results)):
			date = search_results[i].find_element(By.CSS_SELECTOR, 'div.info > div.vitals > div.updated').text.strip().replace('Added ', '')
			date = strptime(date, '%B %d, %Y')
			dates.append(mktime(date))
		
		if (dates == sorted(dates, reverse = True)):
			descending = True
		
		self.assertEqual(True, descending)
		self.assertIn('sort=created', driver.current_url)
	
	def test_that_searching_for_a_tag_returns_results(self):
		driver = self.driver
		driver.get("https://addons.allizom.org/en-US/firefox/")
		load_promomenu = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='promos']")))
		
		search_term = 'development'
		driver.find_element(By.ID, "search-q").send_keys(search_term)
		driver.find_element(By.CSS_SELECTOR, "#search > button").click()
		
		search_count_without_tag = len(driver.find_elements(By.CSS_SELECTOR, "div.items div.item.addon"))
		self.assertGreater(search_count_without_tag , 0)
		
		driver.find_element(By.CSS_SELECTOR, 'li#tag-facets h3').click()
		driver.find_element(By.CSS_SELECTOR, '#tag-facets > ul > li:nth-child(4) > a').click()
		
		self.assertGreaterEqual(search_count_without_tag, len(driver.find_elements(By.CSS_SELECTOR, "div.items div.item.addon")))

	def test_that_verifies_the_url_of_the_statistics_page(self):
		driver = self.driver
		driver.get("https://addons.allizom.org/en-US/firefox/addon/firebug/")
		
		statistics_page = driver.find_element(By.CSS_SELECTOR, '#daily-users > a.stats').click()
		WebDriverWait(driver, 10).until(EC.title_contains("Statistics"))
		self.assertIn('/statistics', driver.current_url)
		
	def test_the_recently_added_section(self):
		driver = self.driver
		descending = False
		driver.get("https://addons.allizom.org/en-US/")
		load_promomenu = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='promos']")))
		
		themes_menu = driver.find_element(By.CSS_SELECTOR, '#themes > a').click()
		recently_added = driver.find_elements(By.CSS_SELECTOR, "#personas-created .persona-small")
		iso_dates = [element.text for element in recently_added]
		
		dates = [datetime.datetime.strptime(s, "Added %B %d, %Y").isoformat()
				for s in iso_dates]
		if (dates == sorted(dates, reverse = True)):
			descending = True
		
		self.assertIn('themes', driver.current_url)
		self.assertEqual(6, len(recently_added))
		self.assertTrue(True, descending)	
		
	def test_that_most_popular_link_is_default(self):
		driver = self.driver
		driver.get("https://addons.allizom.org/en-US/")
		load_promomenu = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='promos']")))
		
		themes_menu = driver.find_element(By.CSS_SELECTOR, '#themes')
		complete_themes_menu = driver.find_element(By.CSS_SELECTOR, '#site-nav div > a.complete-themes > b')
		actions = ActionChains(driver)
		actions.move_to_element(themes_menu).move_to_element(complete_themes_menu).click().perform()
		
		for link in driver.find_elements(By.CSS_SELECTOR, '#side-explore a'):
			link_examined = link.value_of_css_property('font-weight')
			if link_examined == "bold":
				explore_filter = link.text
			if link_examined.isdigit():
				if int((link_examined)) > 400:
					explore_filter = link.text
		
		self.assertTrue(driver.current_url.endswith("/complete-themes/"))
		self.assertEqual(explore_filter, 'Most Popular')
		
	def test_that_external_link_leads_to_addon_website(self):
		driver = self.driver
		driver.get("https://addons.allizom.org/en-US/firefox/addon/memchaser/")
		
		website_link = driver.find_element(By.CSS_SELECTOR, ".links a.home").get_attribute('href')
		results = website_link.split("https")
		result = results[2]
		self.assertTrue(website_link != '')
		
		driver.find_element(By.CSS_SELECTOR, ".links a.home").click()
		self.assertIn(result[4:], driver.current_url)
	
	def test_user_can_access_the_edit_profile_page(self):
		driver = self.driver
		driver.get("https://addons.allizom.org/en-US/firefox/")
		load_promomenu = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='promos']")))
		
		login_page = driver.find_element(By.XPATH, "//*[@id='aux-nav']/ul/li[1]/a[2]").click()
		driver.find_element(By.ID, 'id_username').send_keys('username')
		driver.find_element(By.ID, 'id_password').send_keys('password' + Keys.RETURN)
		user_logged_in_element = driver.find_element(By.CSS_SELECTOR, "#aux-nav .account a.user")
		self.assertTrue('https://addons.allizom.org/en-US/firefox/', driver.current_url)
		self.assertTrue(user_logged_in_element.is_displayed())
		
		hover_profile = driver.find_element(By.CSS_SELECTOR, "#aux-nav .account a.user")
		click_profile = driver.find_element(By.CSS_SELECTOR, "#aux-nav .account ul").find_element(By.CSS_SELECTOR, " li:nth-child(2) a")
		ActionChains(driver).move_to_element(hover_profile).\
                move_to_element(click_profile).\
                click().perform()
		
		self.assertIn("/users/edit", driver.current_url)
		self.assertEqual("My Account", driver.find_element(By.CSS_SELECTOR, "#acct-account > legend").text)
		self.assertEqual("Profile", driver.find_element(By.CSS_SELECTOR, "#profile-personal > legend").text)
		self.assertEqual("Details", driver.find_element(By.CSS_SELECTOR, "#profile-detail > legend").text)
		self.assertEqual("Notifications", driver.find_element(By.CSS_SELECTOR, "#acct-notify > legend").text)
	
	def test_that_make_contribution_button_is_clickable_while_user_is_logged_in(self):
		driver = self.driver
		driver.get("https://addons.allizom.org/en-US/firefox/addon/firebug/")
		
		driver.find_element(By.XPATH, "//*[@id='aux-nav']/ul/li[1]/a[2]").click()
		driver.find_element(By.ID, 'id_username').send_keys('username')
		driver.find_element(By.ID, 'id_password').send_keys('password' + Keys.RETURN)
		user_logged_in_element = driver.find_element(By.CSS_SELECTOR, "#aux-nav .account a.user")
		self.assertTrue(user_logged_in_element.is_displayed())
		
		contribution_snippet = driver.find_element(By.CSS_SELECTOR, "#contribute-button").click()
		self.assertTrue(driver.find_element(By.ID, 'contribute-confirm').is_displayed())
		self.assertEqual("Make Contribution", driver.find_element(By.ID, 'contribute-confirm').text)
	
	def test_the_logout_link_for_logged_in_users(self):
		driver = self.driver
		not_logged = []
		driver.get("https://addons.allizom.org/en-US/")
		load_promomenu = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH, "//*[@id='promos']")))
		
		login_page = driver.find_element(By.XPATH, "//*[@id='aux-nav']/ul/li[1]/a[2]").click()
		driver.find_element(By.ID, 'id_username').send_keys('username')
		driver.find_element(By.ID, 'id_password').send_keys('password' + Keys.RETURN)
		user_logged_in_element = driver.find_element(By.CSS_SELECTOR, "#aux-nav .account a.user")
		self.assertTrue('https://addons.allizom.org/en-US/', driver.current_url)
		self.assertTrue(user_logged_in_element.is_displayed())
		
		discovery_pane = driver.find_element(By.CSS_SELECTOR, '#aux-nav > ul > li.account > a')
		logout_button = driver.find_element(By.CSS_SELECTOR, '#aux-nav > ul > li.account > ul > li.nomenu.logout > a')
		ActionChains(driver).move_to_element(discovery_pane).\
                move_to_element(logout_button).\
                click().perform()
		
		self.assertTrue('https://addons.allizom.org/en-US/', driver.current_url)
		not_logged = driver.find_elements(By.CSS_SELECTOR, "#aux-nav .account a.user")
		self.assertTrue(len(driver.find_elements(By.CSS_SELECTOR, "#aux-nav .account a.user")) == 0)
	
	def tearDown(self):
		self.driver.quit()

if __name__ == "__main__":
    unittest.main()